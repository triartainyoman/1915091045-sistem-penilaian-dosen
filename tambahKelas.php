<?php
include "conn.php";


if (isset($_POST['submit'])) {
  $namaKelas = $_POST['namaKelas'];
  $prodi = $_POST['prodi'];
  $fakultas = $_POST['fakultas'];

  $sql = "INSERT INTO kelas(nama_kelas, prodi, fakultas) VALUES('$namaKelas', '$prodi ', '$fakultas')";

  if (mysqli_query($conn, $sql)) {
    header('Location: kelas.php');
  }
}
?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

  <title>Tambah Kelas | Sistem Penjadwalan Dosen</title>
</head>

<body>
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container">
      <a class="navbar-brand" href="index.php">SIM Dosen</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav ms-auto">
          <a class="nav-link" aria-current="page" href="index.php">Beranda</a>
          <a class="nav-link" href="jadwalKelas.php">Jadwal Kelas</a>
          <a class="nav-link" href="dosen.php">Data Dosen</a>
          <a class="nav-link active" href="kelas.php">Data Kelas</a>
        </div>
      </div>
    </div>
  </nav>

  <div class="container my-5">
    <h1>Tambah Data Kelas</h1>
    <div class="p-5 bg-light rounded">
      <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <div class="mb-3">
          <label for="namaKelas" class="form-label">Nama Kelas</label>
          <input name="namaKelas" type="text" class="form-control" id="namaKelas" required>
        </div>
        <div class="mb-3">
          <label for="prodi" class="form-label">Program Studi</label>
          <input name="prodi" type="text" class="form-control" id="prodi" required>
        </div>
        <div class="mb-3">
          <label for="fakultas" class="form-label">Fakultas</label>
          <input name="fakultas" type="text" class="form-control" id="fakultas" required>
        </div>
        <button type="submit" name="submit" class="btn btn-primary">Tambahkan</button>
        <a href="kelas.php" class="btn btn-danger">Batalkan</a>
      </form>
    </div>
  </div>

  <!-- Optional JavaScript; choose one of the two! -->

  <!-- Option 1: Bootstrap Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

  <!-- Option 2: Separate Popper and Bootstrap JS -->
  <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    -->
</body>

</html>